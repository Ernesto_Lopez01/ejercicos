import java.util.List;
public class FP_Functional_Exercises {
    public static void main(String[] args){
        List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
        
        List<String> courses = List.of ("Spring", "Spring Boot", "API",
                    "Microservices", "AWS", "PCF", "Azure", "Docker", "Kubernetes");
        
        /*---------------------Ejercicio 1 ----------------------*/
        System.out.println("\n Ejercicio #1: ");
        printEvenNumbersInListFunctional(numbers);
        
        /*---------------------Ejercicio 2 ----------------------*/
        System.out.println("\n Ejercicio #2: ");
        printAllCourses(courses);
        
        /*---------------------Ejercicio 3 ----------------------*/
        System.out.println("\n Ejercicio #3: ");
        printSpringCourses(courses);
        
        /*---------------------Ejercicio 4 ----------------------*/
        System.out.println("\n Ejercicio #4: ");
        printEvenList(courses);
        
        /*---------------------Ejercicio 5 ----------------------*/
        System.out.println("\n Ejercicio #5: ");
        printSquaresNumber(numbers);
        
        /*---------------------Ejercicio 6 ----------------------*/
        System.out.println("\n Ejercicio #6: ");
        printcountEvenNumber(courses);     
        
    }
        
   private static  void print(int number){
       System.out.println(number + ", ");
   }
   
   private static void printS(String courses){
       System.out.println(courses+ ", ");
   }
   
   private static boolean isEven(int number){
       return(number % 2 != 0 );
   }
   /*---------------------Ejercicio 1 ----------------------*/
   private static void printEvenNumbersInListFunctional(List<Integer> numbers){
       numbers.stream()
               .filter(FP_Functional_Exercises::isEven)
               .forEach(FP_Functional_Exercises::print);
       System.out.println("");
   }
   
   /*---------------------Ejercicio 2 ----------------------*/
   private static void printAllCourses(List<String> courses){
       courses.stream()
               .forEach(FP_Functional_Exercises::printS);
       System.out.println("");
   }
   
   /*---------------------Ejercicio 3 ----------------------*/
   private static void printSpringCourses(List<String> courses){
       courses.stream()
               .filter(c -> c.contains("Spring"))
               .forEach(FP_Functional_Exercises::printS);
   }
   
   /*---------------------Ejercicio 4 ----------------------*/
   private static void  printEvenList(List<String> courses){
       courses.stream()
               .filter(c -> c.length() >= 4)
               .forEach(FP_Functional_Exercises::printS);
       System.out.println("");
   }
   
   /*---------------------Ejercicio 5 ----------------------*/
   private static void printSquaresNumber(List<Integer> numbers){
       numbers.stream()
               .filter(number -> number % 2 == 1)
               .map(number -> number * number)
               .forEach(FP_Functional_Exercises::print);
       System.out.println("");
   }
   
   /*---------------------Ejercicio 6 ----------------------*/
   private static void printcountEvenNumber(List<String> courses){
       courses.stream()
               .map(c -> c.length())
               .forEach(FP_Functional_Exercises::printS);
       System.out.println("");
   }
}
